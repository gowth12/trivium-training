/**
 * 
 */
package connectionObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * @author Goutham.Himachala This class is used to generate Connection String
 */
public class DbUtill
{
    static Logger logger = Logger.getLogger(DbUtill.class.getName());

    private static final String url = "jdbc:postgresql://localhost:5432/VehicleManagement";

    private static final String user = "postgres";

    private static final String password = "postgres";

    static Connection con = null;

    /**
     * This acts as a private constructor and loads the postgres driver
     * 
     * @throws Exception
     */
    private DbUtill() throws Exception
    {
        Class.forName("org.postgresql.Driver");
    }

    /***
     * This method builds the connection to the database
     * 
     * @return connection String
     */
    public static Connection getConnection()
    {
        try
        {
            new DbUtill();
            con = DriverManager.getConnection(url, user, password);
            if(con == null)
                System.out.println("Connection failed");
            else
                System.out.println("Connection Sucess");
        }
        catch(Exception e)
        {
            System.out.println("Could not connect");
            e.printStackTrace();
        }
        return con;
    }

    /**
     * This method closes the connection from the database
     * 
     * @throws SQLException
     */
    public void closeConnection() throws SQLException
    {
        con.close();
    }
}
