package admin;
/**
 * @author Goutham.Himachala 
 * This class is used to generate pojo objects on admin registration
 */
public class Admin
{
    private String uname;

    private String password;

    public String getUname()
    {
        return uname;
    }

    public void setUname(String uname)
    {
        this.uname = uname;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return "Login [uname=" + uname + ", password=" + password + "]";
    }
}
