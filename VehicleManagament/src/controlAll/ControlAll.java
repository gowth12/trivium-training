package controlAll;

import java.util.Scanner;
import java.util.logging.Logger;

import VehicleManagementTest.VehicleManagementTest;
import autoMobiles.AutoMobiles;
import userRegistration.UserRegistration;
import userRegistration.UserRegistrationDaoImpl;

/**
 * @author Goutham.Himachala
 */
public class ControlAll
{
    static Logger logger = Logger.getLogger(VehicleManagementTest.class.getName());

    static Scanner sc = new Scanner(System.in);

    UserRegistrationDaoImpl urdi = new UserRegistrationDaoImpl();

    static UserRegistration ur = new UserRegistration();

    static AutoMobiles am = new AutoMobiles();

    /**
     * related to user registration
     */
    public static UserRegistration userSeekInserUpdatetData()
    {
        logger.info("enter the phone number");
        long phoneNo = sc.nextLong();
        ur.setPhoneNo(phoneNo);
        logger.info("enter the first name");
        String fName = sc.next();
        ur.setfName(fName);
        logger.info("enter the last name");
        String lName = sc.next();
        ur.setlName(lName);
        logger.info("enter the address");
        String address = sc.next();
        ur.setAddress(address);
        logger.info("enter the gender");
        String gender = sc.next();
        ur.setGender(gender);
        logger.info("enter the dob");
        String dob = sc.next();
        ur.setDob(dob);
        logger.info("enter the driving license Number");
        long drivingLicenseNo = sc.nextLong();
        ur.setDrivingLicenseNo(drivingLicenseNo);
        logger.info("enter the emailId");
        String email = sc.next();
        ur.setEmail(email);
        logger.info("enter the vehicle number");
        String vehicleNo = sc.next();
        ur.setVehicleNo(vehicleNo);
        logger.info("enter the userName");
        String uName = sc.next();
        ur.setuName(uName);
        logger.info("enter the password");
        String password = sc.next();
        ur.setPassword(password);
        return ur;
    }

    public static long userSeekDelAndGetPhone()
    {
        logger.info("enter the phone number");
        long phoneNo = sc.nextLong();
        ur.setPhoneNo(phoneNo);
        phoneNo = ur.getPhoneNo();
        return phoneNo;
    }

    /*
     * related to automobiles
     */
    public static int autoMSeekDelAndGetId()
    {
        logger.info("enter the id");
        int id = sc.nextInt();
        am.setId(id);
        id = am.getId();
        return id;
    }

    public static AutoMobiles autoMSeekInserData()
    {
        logger.info("enter the name");
        String name = sc.next();
        am.setName(name);
        logger.info("enter the vehicletype");
        String vehicletype = sc.next();
        am.setVehicleType(vehicletype);
        logger.info("enter the vehiclemodel");
        int vehiclemodel = sc.nextInt();
        am.setVehicleModel(vehiclemodel);
        logger.info("enter the price");
        double price = sc.nextDouble();
        am.setPrice(price);
        logger.info("enter the productcompany");
        String productcompany = sc.next();
        am.setProductCompany(productcompany);
        logger.info("enter the warranty");
        double warranty = sc.nextDouble();
        am.setWarranty(warranty);
        return am;
    }

    public static AutoMobiles autoMSeekUpdateData()
    {
        logger.info("enter the id");
        int id = sc.nextInt();
        am.setId(id);
        ControlAll.autoMSeekInserData();
        return am;
    }
    
    /**
     * Feed Back table entries
     * 
     */
    
    public static String insert()
    {
        logger.info("enter the message");
        String message = sc.next();
        return message;
        
    }
}
