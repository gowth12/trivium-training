package usedVehicles;
/**
 * @author Goutham.Himachala 
 * This servlet is used to insert the old vehicles
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminInsertOldVehSer
 */
@WebServlet("/AdminInsertOldVehSer")
public class AdminInsertOldVehSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        UsedVehicles uv = new UsedVehicles();
        UsedVehiclesDaoImpl uvdi = new UsedVehiclesDaoImpl();
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        String vehicleName = request.getParameter("txtVName");
        String model = request.getParameter("txtModel");
        String mielage = request.getParameter("txtMielage");
        String doccs = request.getParameter("txtDoccs");
        String condition = request.getParameter("txtCondition");
        String phone = request.getParameter("txtPhone");
        long phone1 = Long.parseLong(phone);
        double mielage1 = Double.parseDouble(mielage);
        uv.setCondition(condition);
        uv.setDoccsAvailable(doccs);
        uv.setMielage(mielage1);
        uv.setModel(model);
        uv.setPhone(phone1);
        uv.setVname(vehicleName);
        try
        {
            String msg = uvdi.create(uv);
            if(msg == "sucessfully added")
                response.sendRedirect("AChoice.html");
            else
                pw.print("failed to add");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
}
