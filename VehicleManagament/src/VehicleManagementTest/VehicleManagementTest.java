/**
 * 
 */
package VehicleManagementTest;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import autoMobiles.AutoMobilesDaoImpl;
import feedBack.FeedBackDaoImpl;
import serviceList.ServiceList;
import serviceList.ServiceListDaoImpl;
import usedVehicles.UsedVehicles;
import usedVehicles.UsedVehiclesDaoImpl;
import userRegistration.UserRegistration;
import userRegistration.UserRegistrationDaoImpl;

/**
 * @author Goutham.Himachala This is the main class
 */
public class VehicleManagementTest
{
    Scanner sc = new Scanner(System.in);

    static Logger logger = Logger.getLogger(VehicleManagementTest.class.getName());

    static FeedBackDaoImpl fbdi = new FeedBackDaoImpl();

    static AutoMobilesDaoImpl am = new AutoMobilesDaoImpl();

    static UserRegistrationDaoImpl ur = new UserRegistrationDaoImpl();

    static UserRegistration u = new UserRegistration();

    static ServiceListDaoImpl sldi = new ServiceListDaoImpl();

    static ServiceList sl = new ServiceList();

    /**
     * main method
     */
    public static void main(String[] args)
    {
        UsedVehicles uv = new UsedVehicles();
        UsedVehiclesDaoImpl uvdi = new UsedVehiclesDaoImpl();
        uv.setCondition("y");
        uv.setDoccsAvailable("yth");
        uv.setMielage(2.22);
        uv.setModel("44");
        uv.setPhone(1111);
        uv.setVname("ujyhtg");
        try
        {
            String msg = uvdi.create(uv);
            if(msg == "sucessfully added")
                System.out.println(msg);
            else
                System.out.println(msg);
            List<UsedVehicles> u = uvdi.show();
            System.out.println(u);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
}
