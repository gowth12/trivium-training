package userRegistration;

/**
 * @author Goutham.Himachala
 *  This servlet creates the new user
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class NewUserSer
 */
@WebServlet("/NewUserSer")
public class NewUserSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    UserRegistrationDaoImpl nur = new UserRegistrationDaoImpl();

    UserRegistration ur = new UserRegistration();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String phoneNo = request.getParameter("phoneNo");
        long phoneNoC = Long.parseLong(phoneNo);
        ur.setPhoneNo(phoneNoC);
        String fName = request.getParameter("fname");
        ur.setfName(fName);
        String lName = request.getParameter("lname");
        ur.setlName(lName);
        String address = request.getParameter("address");
        ur.setAddress(address);
        String gender = request.getParameter("gender");
        ur.setGender(gender);
        String dob = request.getParameter("dob");
        ur.setDob(dob);
        String vehicleNo = request.getParameter("vehicleNo");
        ur.setVehicleNo(vehicleNo);
        String email = request.getParameter("email");
        ur.setEmail(email);
        String drivingLicenseNo = request.getParameter("dlNo");
        long drivingLicenseNoC = Long.parseLong(drivingLicenseNo);
        ur.setDrivingLicenseNo(drivingLicenseNoC);
        String uName = request.getParameter("uname");
        ur.setuName(uName);
        String password = request.getParameter("password");
        ur.setPassword(password);
        // out.print(ur);
        try
        {
            String res = nur.create(ur);
            out.print(res);
            if(res == "sucess")
            {
                response.sendRedirect("ULogin.html");
            }
            else
            {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('User or password incorrect');");
                out.println("</script>");
                response.sendRedirect("NewUser.html");
            }
        }
        catch(SQLException e)
        {
            e.getMessage();
            e.printStackTrace();
        }
    }
}
