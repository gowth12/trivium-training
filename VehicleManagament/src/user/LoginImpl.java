package user;

/**
 * @author Goutham.Himachala 
 * This class checks the userId and password of user with the database
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import connectionObject.DbUtill;

public class LoginImpl
{
    PreparedStatement pstmt = null;

    Connection con = DbUtill.getConnection();

    Login l = new Login();

    public boolean validateLogin(Login l) throws SQLException
    {
        String sql = "select * from UserRegistration where uName=?and password=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setString(1, l.getUname());
        pstmt.setString(2, l.getPassword());
        ResultSet rs = pstmt.executeQuery();
        if(rs.next())
            return true;
        else
            return false;
    }
}
