package userRegistration;

/**
 * @author Goutham.Himachala
 *  This servlet deletes the  user based on phoneNo
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class NewUserSer
 */
@WebServlet("/AdminDeleteUser")
public class AdminDeleteUser extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        UserRegistration u = new UserRegistration();
        UserRegistrationDaoImpl urdi = new UserRegistrationDaoImpl();
        String phoneNo = request.getParameter("txtDelete");
        long phone = Long.parseLong(phoneNo);
        u.setPhoneNo(phone);
        phone = u.getPhoneNo();
        try
        {
            String msg = urdi.delete(phone);
            if(msg == "sucessfully deleted")
            {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Sucessfully deleted!!');");
                out.println("location='AChoice.html';");
                out.println("</script>");
                //response.sendRedirect("AChoice.html");
            }
            else
                out.println("<script type=\"text/javascript\">");
            out.println("alert('failed to delete!!');");
            out.println("location='AChoice.html';");
            out.println("</script>");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
