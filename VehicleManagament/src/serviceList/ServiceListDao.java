package serviceList;

import java.sql.SQLException;
import java.util.List;

public interface ServiceListDao
{
    
    /**
     * This method is used to retireve all the used vehicles
     * 
     * @return a list whic contains all the records
     * @throws SQLException
     */
    public List<ServiceList> show() throws SQLException;

    /**
     * This methos deletes the data by accepting the unique id
     * 
     * @param id:
     *            accepts unique id of the UsedVehicles
     * @return String sucess or failure message
     * @throws SQLException
     * @throws Exception
     */
    public String delete(int id) throws Exception;

    /**
     * This method is used to retireve the list of auto mobile parts based on phoneNo
     * 
     * @param id:accepts
     *            unique id of the user
     * @return one record based on the id passed
     * @throws SQLException
     */
    public List<ServiceList> getById(int id) throws SQLException;

    /**
     * This method is used for the updating the existing data with new data for the particular user
     * 
     * @param sl:accepts
     *            inputs from user of type ServiceList
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String update(ServiceList sl) throws SQLException;

    /**
     * This method inserts the data in to the table ServiceList
     * 
     * @param sl:accpets
     *            inputs from user of type ServiceList
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String create(ServiceList sl) throws SQLException;
    
    
    /**
     * This methos deletes the data by accepting the unique id
     * 
     * @param id:
     *            accepts unique id of the UsedVehicles
     * @return String sucess or failure message
     * @throws SQLException
     * @throws Exception
     */
    public String deleteByPhone(long id) throws Exception;
}


