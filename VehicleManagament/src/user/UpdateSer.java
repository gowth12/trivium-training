package user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import userRegistration.UserRegistration;
import userRegistration.UserRegistrationDaoImpl;

/**
 * Servlet implementation class UpdateSer
 */
@WebServlet("/UpdateSer")
public class UpdateSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();
        UserRegistrationDaoImpl urdi = new UserRegistrationDaoImpl();
        UserRegistration ur = new UserRegistration();
        String phoneNo = request.getParameter("phoneNo");
        long phoneNoC = Long.parseLong(phoneNo);
        ur.setPhoneNo(phoneNoC);
        String fName = request.getParameter("fname");
        ur.setfName(fName);
        String lName = request.getParameter("lname");
        ur.setlName(lName);
        String address = request.getParameter("address");
        ur.setAddress(address);
        String gender = request.getParameter("gender");
        ur.setGender(gender);
        String dob = request.getParameter("dob");
        ur.setDob(dob);
        String vehicleNo = request.getParameter("vehicleNo");
        ur.setVehicleNo(vehicleNo);
        String email = request.getParameter("email");
        ur.setEmail(email);
        String drivingLicenseNo = request.getParameter("dlNo");
        long drivingLicenseNoC = Long.parseLong(drivingLicenseNo);
        ur.setDrivingLicenseNo(drivingLicenseNoC);
        String uName = request.getParameter("uname");
        ur.setuName(uName);
        String password = request.getParameter("password");
        ur.setPassword(password);
        String msg;
        try
        {
            msg = urdi.update(ur);
            if(msg == "sucessfully updated")
            {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Sucessfully updated!!');");
                out.println("location='ULogin.html';");
                out.println("</script>");
            }
            else
            {
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
}
