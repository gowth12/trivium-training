package usedVehicles;
/**
 * @author Goutham.Himachala 
 * This servlet is used to delete the old vehicles
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminDeleteVehicleSer
 */
@WebServlet("/AdminDeleteVehicleSer")
public class AdminDeleteVehicleSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        UsedVehicles uv = new UsedVehicles();
        UsedVehiclesDaoImpl uvdi = new UsedVehiclesDaoImpl();
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        String id = request.getParameter("txtDelete");
        int id1 = Integer.parseInt(id);
        uv.setId(id1);
        try
        {
            String msg = uvdi.delete(uv.getId());
            if(msg == "sucessfully deleted")
                response.sendRedirect("AChoice.html");
            else
                pw.print("Failed to delete");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
