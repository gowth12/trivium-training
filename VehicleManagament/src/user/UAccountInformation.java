package user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import userRegistration.UserRegistration;
import userRegistration.UserRegistrationDaoImpl;

/**
 * Servlet implementation class UAccountInformation
 */
@WebServlet("/UAccountInformation")
public class UAccountInformation extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PrintWriter pw = response.getWriter();
        UserRegistrationDaoImpl urdi = new UserRegistrationDaoImpl();
        HttpSession session = request.getSession(false);
        String name = (String)session.getAttribute("uname");
        try
        {
            List<UserRegistration> ur = urdi.individual(name);
            if(!ur.isEmpty())
            {
                for(UserRegistration a : ur)
                {
                    pw.println("<html><body BGcolor=#99a69c>");
                    pw.println("<TABLE BORDER=1 height=100  style=\"margin:100px; height:150px; width:250px\" cellspacing=\"2px\">");
                    pw.println("<TR><TH>UName</TH><TH>FName</TH> <TH>LName</TH> <TH>Password</TH><TH>Gender</th>"
                            + "<TH>Address</TH><TH>Email</TH><TH>VehicleNo</TH><TH>DlNo</TH><TH>PhoneNO"
                            + "</TH><TH>DoB</TH></TR>");
                    pw.println("<TR><TD>" + a.getuName() + "</TD><TD>" + a.getfName() + "</TD><TD>" + a.getlName()
                            + "</td><TD>" + a.getPassword() + "</TD><TD>" + a.getGender() + "</TD><TD>" + a.getAddress()
                            + "</TD><TD>" + a.getEmail() + "</TD><TD>" + a.getVehicleNo() + "</TD><TD>"
                            + a.getDrivingLicenseNo() + "</TD><TD>" + a.getPhoneNo() + "</TD><TD>" + a.getDob()
                            + "</TD></TR>");
                    pw.print("</table>");
                    pw.print("<a style=color:blue href=UChoice.html>Home</a><br>");
                    pw.print("<a style=color:blue href=UUpdateAccount.jsp>Edit Account</a>");
                    pw.println("</html></body>");
                }
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        response.setContentType("text/html");
    }
}
