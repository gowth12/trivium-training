package serviceList;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserDeleteServiceSer
 */
@WebServlet("/UserDeleteServiceSer")
public class UserDeleteServiceSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ServiceList sl = new ServiceList();
        ServiceListDaoImpl sldi = new ServiceListDaoImpl();
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        String id = request.getParameter("txtDelete");
        int id1 = Integer.parseInt(id);
        sl.setId(id1);
        try
        {
            String msg = sldi.deleteByPhone(sl.getId());
            if(msg == "sucessfully deleted")
                response.sendRedirect("UChoice.html");
            else
                pw.print("Failed to delete");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
