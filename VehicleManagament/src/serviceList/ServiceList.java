package serviceList;

public class ServiceList
{
    private int id;

    private String Uname;
    private long phone;
    private String address;
    private String pickUpDate;
    private String pickTime;
    private String cashVia;
    
    
    @Override
    public String toString()
    {
        return "ServiceList [id=" + id + ", Uname=" + Uname + ", phone=" + phone + ", address=" + address
                + ", pickUpDate=" + pickUpDate + ", pickTime=" + pickTime + ", cashVia=" + cashVia + "]";
    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getUname()
    {
        return Uname;
    }
    public void setUname(String uname)
    {
        Uname = uname;
    }
    public long getPhone()
    {
        return phone;
    }
    public void setPhone(long phone)
    {
        this.phone = phone;
    }
    public String getAddress()
    {
        return address;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }
    public String getPickUpDate()
    {
        return pickUpDate;
    }
    public void setPickUpDate(String pickUpDate)
    {
        this.pickUpDate = pickUpDate;
    }
    public String getPickTime()
    {
        return pickTime;
    }
    public void setPickTime(String pickTime)
    {
        this.pickTime = pickTime;
    }
    public String getCashVia()
    {
        return cashVia;
    }
    public void setCashVia(String cashVia)
    {
        this.cashVia = cashVia;
    }
    
}
