/**
 * 
 */
package autoMobiles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connectionObject.DbUtill;

/**
 * This class is used to perform opertaion on UserRegistration
 * 
 * @author Goutham.Himachala
 */
public class AutoMobilesDaoImpl implements AutoMobilesDao
{
    Connection con = DbUtill.getConnection();

    PreparedStatement pstmt = null;

    List<AutoMobiles> list = new ArrayList<AutoMobiles>();

    AutoMobiles am = new AutoMobiles();

    String sql;

    /**
     * This method is used to retireve all the list of auto mobiles
     */
    @Override
    public List<AutoMobiles> show() throws SQLException
    {
        sql = "SELECT id, name, vehicletype, vehiclemodel, price, productcompany, warranty FROM automobiles";
        Statement stmt = con.createStatement();
        // setting up the parameter's for the Query string
        ResultSet rs = stmt.executeQuery(sql);
        while(rs.next())
        {
            am = new AutoMobiles();
            am.setId(rs.getInt(1));
            am.setName(rs.getString(2));
            am.setVehicleType(rs.getString(3));
            am.setVehicleModel(rs.getInt(4));
            am.setPrice(rs.getDouble(5));
            am.setProductCompany(rs.getString(6));
            am.setWarranty(rs.getDouble(7));
            list.add(am);
        }
        return list;
    }

    /**
     * This methos deletes the data by accepting the unique id
     */
    @Override
    public String delete(int id) throws Exception
    {
        sql = "DELETE FROM automobiles WHERE id=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setInt(1, id);
        if(pstmt.executeUpdate() != 0)
            return "sucessfully deleted";
        else
            return "failed to delete";
    }

    /**
     * This method is used to retireve the list of auto mobile parts based on ID
     */
    @Override
    public List<AutoMobiles> getById(int id) throws SQLException
    {
        sql = "SELECT id, name, vehicletype, vehiclemodel, price, productcompany, warranty FROM automobiles where id=?";
        PreparedStatement pstmt = con.prepareStatement(sql);
        pstmt.setInt(1, id);
        // setting up the parameter's for the Query string
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            am.setId(rs.getInt(1));
            am.setName(rs.getString(2));
            am.setVehicleType(rs.getString(3));
            am.setVehicleModel(rs.getInt(4));
            am.setPrice(rs.getDouble(5));
            am.setProductCompany(rs.getString(6));
            am.setWarranty(rs.getDouble(7));
            list.add(am);
        }
        return list;
    }

    /**
     * This method is used for the updating the existing data with new data for the particular spare
     * part
     */
    @Override
    public String update(AutoMobiles am) throws SQLException
    {
        sql = "UPDATE automobiles SET name=?, vehicletype=?, vehiclemodel=?, price=?, productcompany=?, warranty=? WHERE id=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setString(1, am.getName());
        pstmt.setString(2, am.getVehicleType());
        pstmt.setInt(3, am.getVehicleModel());
        pstmt.setDouble(4, am.getPrice());
        pstmt.setString(5, am.getProductCompany());
        pstmt.setDouble(6, am.getWarranty());
        pstmt.setInt(7, am.getId());
        if(pstmt.executeUpdate() != 0)
            return "sucessfully updated";
        else
            return "failed to update";
    }

    /**
     * This method inserts the data in to the table AutoMobiles
     */
    @Override
    public String create(AutoMobiles am) throws SQLException
    {
        sql = "INSERT INTO automobiles(name, vehicletype, vehiclemodel, price, productcompany, warranty)VALUES (?, ?, ?, ?, ?, ?)";
        pstmt = con.prepareStatement(sql);
        pstmt.setString(1, am.getName());
        pstmt.setString(2, am.getVehicleType());
        pstmt.setInt(3, am.getVehicleModel());
        pstmt.setDouble(4, am.getPrice());
        pstmt.setString(5, am.getProductCompany());
        pstmt.setDouble(6, am.getWarranty());
        if(pstmt.executeUpdate() != 0)
            return "sucessfully added";
        else
            return "failed to add";
    }
}
