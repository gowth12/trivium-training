package user;

/**
 * @author Goutham.Himachala 
 * This servlet checks the user name and the password of the user
 * with the database abd redirects to other form, if 
 * it is valid
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginnSer
 */
@WebServlet("/LoginnSer")
public class LoginnSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        LoginImpl li = new LoginImpl();
        Login l = new Login();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String uname = request.getParameter("uname");
        String password = request.getParameter("password");
        l.setUname(uname);
        l.setPassword(password);
        try
        {
            boolean res = li.validateLogin(l);
            if(res == true)
            {
                HttpSession session = request.getSession();
                session.setAttribute("uname", uname);
                response.sendRedirect("UChoice.html");
                out.close();
            }
            else
            {
              
                   out.println("<script type=\"text/javascript\">");
                   out.println("alert('User or password incorrect');");
                   out.println("location='ULogin.html';");
                   out.println("</script>");
                 //  response.sendRedirect("ULogin.html");
                }
              
            
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        out.close();
    }
}
