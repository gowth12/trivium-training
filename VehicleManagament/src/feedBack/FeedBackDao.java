package feedBack;

import java.sql.SQLException;
import java.util.List;

import userRegistration.UserRegistration;

public interface FeedBackDao
{
    public List<UserFeedBack> show() throws SQLException;

    public String create(UserFeedBack ub) throws SQLException;

    public List<UserFeedBack> getById(long phoneNo) throws SQLException;
}
