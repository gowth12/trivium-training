package serviceList;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminDeleteServiceSer
 */
@WebServlet("/AdminDeleteServiceSer")
public class AdminDeleteServiceSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ServiceList sl = new ServiceList();
        ServiceListDaoImpl sldi = new ServiceListDaoImpl();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String id = request.getParameter("txtDelete");
        int id1 = Integer.parseInt(id);
        sl.setId(id1);
        try
        {
            String msg = sldi.delete(sl.getId());
            if(msg == "sucessfully deleted")
            {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Deleted sucessfully');");
                out.println("location='AChoice.html';");
                out.println("</script>");
               
            }
            else
            {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('failed to delete');");
                out.println("location='ADeleteService.html';");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
