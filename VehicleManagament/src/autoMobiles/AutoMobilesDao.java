package autoMobiles;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Goutham.Himachala This class is used as dao domain
 */
public interface AutoMobilesDao
{
    /**
     * This method is used to retireve all the list of auto mobiles
     * 
     * @return a list whic contains all the records
     * @throws SQLException
     */
    public List<AutoMobiles> show() throws SQLException;

    /**
     * This methos deletes the data by accepting the unique id
     * 
     * @param id:
     *            accepts unique id of the autoMobile
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String delete(int id) throws Exception;

    /**
     * This method is used to retireve the list of auto mobile parts based on ID
     * 
     * @param id:accepts
     *            unique id of the autoMobile
     * @return single ecord for the id passed
     * @throws SQLException
     */
    public List<AutoMobiles> getById(int id) throws SQLException;

    /**
     * This method is used for the updating the existing data with new data for the particular spare
     * part
     * 
     * @param id:accepts
     *            inputs from user of type AutoMobiles
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String update(AutoMobiles am) throws SQLException;

    /**
     * This method inserts the data in to the table AutoMobiles
     * 
     * @param am:accpets
     *            inputs from user of type AutoMobiles
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String create(AutoMobiles am) throws SQLException;
}
