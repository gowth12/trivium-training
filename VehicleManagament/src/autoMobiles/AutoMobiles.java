/**
 * 
 */
package autoMobiles;

/**
 * @author Goutham.Himachala 
 * This class is used to generate pojo objects on autoMobiles
 */
public class AutoMobiles
{
    private int id;

    private String name;

    private String vehicleType;

    private int vehicleModel;

    private double price;

    private String productCompany;

    private double warranty;

    /**
     * overriding the to string method whic is in object class
     */
    @Override
    public String toString()
    {
        return "AutoMobiles [id=" + id + ", name=" + name + ", vehicleType=" + vehicleType + ", vehicleModel="
                + vehicleModel + ", price=" + price + ", productCompany=" + productCompany + ", warranty=" + warranty
                + "]";
    }

    /***
     * These methods is used to set and get the value as an object
     */
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getVehicleType()
    {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType)
    {
        this.vehicleType = vehicleType;
    }

    public int getVehicleModel()
    {
        return vehicleModel;
    }

    public void setVehicleModel(int vehiclemodel)
    {
        this.vehicleModel = vehiclemodel;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public String getProductCompany()
    {
        return productCompany;
    }

    public void setProductCompany(String productCompany)
    {
        this.productCompany = productCompany;
    }

    public double getWarranty()
    {
        return warranty;
    }

    public void setWarranty(double warranty)
    {
        this.warranty = warranty;
    }
}
