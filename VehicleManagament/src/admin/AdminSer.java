package admin;

/**
 * @author Goutham.Himachala 
 * This servlet checks the user name and the password of the admin
 * with the database abd redirects to other form, if 
 * it is valid
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Servlet implementation class AdminSer
 */
@WebServlet("/AdminSer")
public class AdminSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    /**
     * This method requets the user name and password and passes to the method ValidateLogin
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        AdminImpl ai = new AdminImpl();
        Admin a = new Admin();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        /*
         * requesting user input
         */
        String uname = request.getParameter("uname");
        String password = request.getParameter("password");
        /*
         * settin up the user values
         */
        a.setUname(uname);
        a.setPassword(password);
        try
        {
            /*
             * calling the method
             */
            boolean res = ai.validateLogin(a);
            if(res == true)
            {
                response.sendRedirect("AChoice.html");
            }
            else
            {
                response.sendRedirect("ALogin.html");
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        out.close();
    }
}
