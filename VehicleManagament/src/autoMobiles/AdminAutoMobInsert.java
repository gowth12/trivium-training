package autoMobiles;

/**
 *  @author Goutham.Himachala 
 * This class accepts the data(auto mobile) from the admin and inserts into the database
 */
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet inserts automobile in to the table 
 * by accepting values form the admin
 */
@WebServlet("/AdminAutoMobInsert")
public class AdminAutoMobInsert extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        AutoMobiles am = new AutoMobiles();
        AutoMobilesDaoImpl amdi = new AutoMobilesDaoImpl();
        response.setContentType("text/html");
        String name = request.getParameter("txtName");
        String vType = request.getParameter("txtVehicleType");
        String vModel = request.getParameter("txtVehicleModel");
        String price = request.getParameter("txtPrice");
        String prodComp = request.getParameter("txtProductCompany");
        String warranty = request.getParameter("txtWarranty");
        double warranty1 = Double.parseDouble(warranty);
        double price1 = Double.parseDouble(price);
        int model1 = Integer.parseInt(vModel);
        am.setWarranty(warranty1);
        am.setPrice(price1);
        am.setName(name);
        am.setVehicleType(vType);
        am.setVehicleModel(model1);
        am.setProductCompany(prodComp);
        try
        {
            String msg = amdi.create(am);
            if(msg == "sucessfully added")
                response.sendRedirect("AChoice.html");
        }
        catch(SQLException e)
        {
            e.getMessage();
        }
    }
}
