package feedBack;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class UFeedBack
 */
@WebServlet("/UFeedBack")
public class UFeedBack extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    FeedBackDaoImpl ufi=new FeedBackDaoImpl();
	    UserFeedBack uf=new UserFeedBack();
	response.setContentType("text/html");
	PrintWriter pw=response.getWriter();
	HttpSession s = request.getSession(false);
    String name = (String)s.getAttribute("uname");
	String phone=request.getParameter("txtNo");
	String msg=request.getParameter("txtMsg");
	long phoneNo=Long.parseLong(phone);
	uf.setUname(name);
	uf.setPhoneNo(phoneNo);
	uf.setMessage(msg);
	try
    {
       String res= ufi.create(uf);
        if(res=="sucessfully added")
        {
            pw.println("<script type=\"text/javascript\">");
            pw.println("alert('Sucessfully added');");
            pw.println("location='UChoice.html';");
            pw.println("</script>");
        }
        else
        {
            pw.println("<script type=\"text/javascript\">");
            pw.println("alert('failed to add');");
            pw.println("location='UChoice.html';");
            pw.println("</script>");
        }
    }
    catch(SQLException e)
    {
     
        e.printStackTrace();
    }
	
	}

}
