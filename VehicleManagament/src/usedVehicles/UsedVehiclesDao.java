package usedVehicles;
/**
 * @author Goutham.Himachala 
 * This class is used as dao domain
 */
import java.sql.SQLException;
import java.util.List;

public interface UsedVehiclesDao
{
    /**
     * This method is used to retireve all the used vehicles
     * 
     * @return a list whic contains all the records
     * @throws SQLException
     */
    public List<UsedVehicles> show() throws SQLException;

    /**
     * This methos deletes the data by accepting the unique id
     * 
     * @param id:
     *            accepts unique id of the UsedVehicles
     * @return String sucess or failure message
     * @throws SQLException
     * @throws Exception
     */
    public String delete(int id) throws Exception;

    /**
     * This method is used to retireve the list of auto mobile parts based on id
     * 
     * @param id:accepts
     *            unique id of the user
     * @return one record based on the id passed
     * @throws SQLException
     */
    public List<UsedVehicles> getById(int id) throws SQLException;

    /**
     * This method is used for the updating the existing data with new data for the particular user
     * 
     * @param ur:accepts
     *            inputs from user of type UsedVehicles
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String update(UsedVehicles uv) throws SQLException;

    /**
     * This method inserts the data in to the table UserRegistration
     * 
     * @param uv:accpets
     *            inputs from user of type UsedVehicles
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String create(UsedVehicles uv) throws SQLException;
}
