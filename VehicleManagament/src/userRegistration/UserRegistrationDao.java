package userRegistration;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Goutham.Himachala 
 * This class is used as dao domain
 */
public interface UserRegistrationDao
{
    /**
     * This method is used to retireve all the list of users
     * 
     * @return a list whic contains all the records
     * @throws SQLException
     */
    public List<UserRegistration> show() throws SQLException;

    /**
     * This methos deletes the data by accepting the unique PhoneNo
     * 
     * @param PhoneNo:
     *            accepts unique PhoneNo of the UserRegistration
     * @return String sucess or failure message
     * @throws SQLException
     * @throws Exception
     */
    public String delete(long phoneNo) throws Exception;

    /**
     * This method is used to retireve the list of auto mobile parts based on phoneNo
     * 
     * @param PhoneNo:accepts
     *            unique phoneNo of the user
     * @return one record based on the phoneNo passed
     * @throws SQLException
     */
    public List<UserRegistration> getByPhoneNo(long phoneNo) throws SQLException;

    /**
     * This method is used for the updating the existing data with new data for the particular user
     * 
     * @param ur:accepts
     *            inputs from user of type UserRegistration
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String update(UserRegistration ur) throws SQLException;

    /**
     * This method inserts the data in to the table UserRegistration
     * 
     * @param ur:accpets
     *            inputs from user of type UserRegistration
     * @return String sucess or failure message
     * @throws SQLException
     */
    public String create(UserRegistration ur) throws SQLException;
}
