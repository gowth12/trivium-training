package feedBack;

public class UserFeedBack
{
    private String message;
    private String uname;
    private long phoneNo;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getUname()
    {
        return uname;
    }

    public void setUname(String uname)
    {
        this.uname = uname;
    }

    public long getPhoneNo()
    {
        return phoneNo;
    }

    public void setPhoneNo(long phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    @Override
    public String toString()
    {
        return "UserFeedBack [message=" + message + ", uname=" + uname + ", phoneNo=" + phoneNo + "]";
    }


    
}
