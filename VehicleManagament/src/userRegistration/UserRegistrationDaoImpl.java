/**
 * 
 */
package userRegistration;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connectionObject.DbUtill;

/**
 * This class is used to perform opertaion on UserRegistration
 * 
 * @author Goutham.Himachala
 */
public class UserRegistrationDaoImpl implements UserRegistrationDao
{
    Connection con = DbUtill.getConnection();

    UserRegistration ur = new UserRegistration();

    PreparedStatement pstmt = null;

    String sql;

    List<UserRegistration> list = new ArrayList<UserRegistration>();

    /**
     * This method inserts the data or value in to the table
     * 
     * @throws SQLException
     */
    @Override
    public String create(UserRegistration ur) throws SQLException
    {
        // Sql statement
        sql = "INSERT INTO userregistration(uname, fname, lname, password, gender, address, email, vehicleno, drivinglicenseno, phoneno, dob)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        // preparing the sql statement
        pstmt = con.prepareStatement(sql);
        // setting up the parameter's to the sql query
        pstmt.setLong(10, ur.getPhoneNo());
        pstmt.setString(1, ur.getuName());
        pstmt.setString(2, ur.getfName());
        pstmt.setString(3, ur.getlName());
        pstmt.setString(4, ur.getPassword());
        pstmt.setString(11, ur.getDob());
        pstmt.setString(5, ur.getGender());
        pstmt.setString(6, ur.getAddress());
        pstmt.setString(7, ur.getEmail());
        pstmt.setString(8, ur.getVehicleNo());
        pstmt.setLong(9, ur.getDrivingLicenseNo());
        // executing the sql statement
        if(pstmt.executeUpdate() > 0)
            return "sucess";
        else
            return "failure";
    }

    /**
     * This gives the list of all the registered users
     */
    @Override
    public List<UserRegistration> show() throws SQLException
    {
        // sql Statement to list out all th registered users
        sql = "select * from UserRegistration";
        Statement stmt = con.createStatement();
        // etracting the result set
        ResultSet rs = stmt.executeQuery(sql);
        while(rs.next())
        {
            ur = new UserRegistration();
            ur.setPhoneNo(rs.getLong(10));
            ur.setuName(rs.getString(1));
            ur.setfName(rs.getString(2));
            ur.setlName(rs.getString(3));
            ur.setPassword(rs.getString(4));
            ur.setDob(rs.getString(11));
            ur.setGender(rs.getString(5));
            ur.setAddress(rs.getString(6));
            ur.setEmail(rs.getString(7));
            ur.setVehicleNo(rs.getString(8));
            ur.setDrivingLicenseNo(rs.getLong(9));
            // adding the object 'ur' to the list
            list.add(ur);
        }
        return list;
    }

    @Override
    public String delete(long phoneNo) throws Exception
    {
        // sql statement to delete the particular user
        sql = "DELETE FROM userregistration WHERE phoneNo=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setLong(1, phoneNo);
        if(pstmt.executeUpdate() != 0)
            return "sucessfully deleted";
        else
            return "failed to delete";
    }

    @Override
    public List<UserRegistration> getByPhoneNo(long phoneNo) throws SQLException
    {
        sql = "select * from UserRegistration where phoneNo=?";
        pstmt = con.prepareStatement(sql);
        // setting up the parameter's for the Query string
        pstmt.setLong(1, phoneNo);
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            ur.setPhoneNo(rs.getLong(10));
            ur.setuName(rs.getString(1));
            ur.setfName(rs.getString(2));
            ur.setlName(rs.getString(3));
            ur.setPassword(rs.getString(4));
            ur.setDob(rs.getString(11));
            ur.setGender(rs.getString(5));
            ur.setAddress(rs.getString(6));
            ur.setEmail(rs.getString(7));
            ur.setVehicleNo(rs.getString(8));
            ur.setDrivingLicenseNo(rs.getLong(9));
            list.add(ur);
        }
        return list;
    }

    public List<UserRegistration> individual(String uname) throws SQLException
    {
        sql = "select * from UserRegistration where uname=?";
        pstmt = con.prepareStatement(sql);
        // setting up the parameter's for the Query string
        pstmt.setString(1, uname);
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            ur.setPhoneNo(rs.getLong(10));
            ur.setuName(rs.getString(1));
            ur.setfName(rs.getString(2));
            ur.setlName(rs.getString(3));
            ur.setPassword(rs.getString(4));
            ur.setDob(rs.getString(11));
            ur.setGender(rs.getString(5));
            ur.setAddress(rs.getString(6));
            ur.setEmail(rs.getString(7));
            ur.setVehicleNo(rs.getString(8));
            ur.setDrivingLicenseNo(rs.getLong(9));
            list.add(ur);
        }
        return list;
    }

    @Override
    public String update(UserRegistration ur) throws SQLException
    {
        sql = "UPDATE userRegistration SET uname=?, fname=?, lname=?, password=?, gender=?, address=?, email=?, vehicleno=?, drivinglicenseno=?, dob=? where phoneNo=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setLong(10, ur.getPhoneNo());
        pstmt.setLong(11, ur.getPhoneNo());
        pstmt.setString(1, ur.getuName());
        pstmt.setString(2, ur.getfName());
        pstmt.setString(3, ur.getlName());
        pstmt.setString(4, ur.getPassword());
        pstmt.setString(10, ur.getDob());
        pstmt.setString(5, ur.getGender());
        pstmt.setString(6, ur.getAddress());
        pstmt.setString(7, ur.getEmail());
        pstmt.setString(8, ur.getVehicleNo());
        pstmt.setLong(9, ur.getDrivingLicenseNo());
        if(pstmt.executeUpdate() != 0)
            return "sucessfully updated";
        else
            return "failed to update";
    }
}
