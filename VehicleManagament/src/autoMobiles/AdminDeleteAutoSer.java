package autoMobiles;

/**
 *  @author Goutham.Himachala 
 * This class accepts the data(auto mobile) from the admin and deletes into the database
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminDeleteAutoSer
 */
@WebServlet("/AdminDeleteAutoSer")
public class AdminDeleteAutoSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        AutoMobiles am = new AutoMobiles();
        AutoMobilesDaoImpl amdi = new AutoMobilesDaoImpl();
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        String id = request.getParameter("txtDelete");
        int id1 = Integer.parseInt(id);
        am.setId(id1);
        try
        {
            String msg = amdi.delete(am.getId());
            if(msg == "sucessfully deleted")
                response.sendRedirect("AChoice.html");
            else
                pw.print("Failed to delete");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
