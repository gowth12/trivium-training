package feedBack;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connectionObject.DbUtill;
import userRegistration.UserRegistration;

public class FeedBackDaoImpl implements FeedBackDao
{
    Connection con = DbUtill.getConnection();

    UserFeedBack ufb = new UserFeedBack();

    UserRegistration ur = new UserRegistration();

    List<UserFeedBack> list = new ArrayList<UserFeedBack>();

    PreparedStatement pstmt = null;

    String sql;

    @Override
    public String create(UserFeedBack ub) throws SQLException
    {
        sql = "insert into feedBack(uname,phoneNo,message)values(?,?,?)";
        pstmt = con.prepareStatement(sql);
        pstmt.setString(1, ub.getUname());
        pstmt.setLong(2, ub.getPhoneNo());
        pstmt.setString(3, ub.getMessage());
        if(pstmt.executeUpdate() != 0)
            return "sucessfully added";
        else
            return "failed to add";
    }

    @Override
    public List<UserFeedBack> show() throws SQLException
    {
        sql = "select * from feedBack";
        pstmt = con.prepareStatement(sql);
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            ufb = new UserFeedBack();
            ufb.setMessage(rs.getString(3));
            ufb.setUname(rs.getString(1));
            ufb.setPhoneNo(rs.getLong(2));
            list.add(ufb);
        }
        return list;
    }

    public List<UserFeedBack> getById(long phoneNo) throws SQLException
    {
        sql = "SELECT * FROM feedBack where phoneNo=?";
        PreparedStatement pstmt = con.prepareStatement(sql);
        pstmt.setLong(1, phoneNo);
        // setting up the parameter's for the Query string
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            ufb = new UserFeedBack();
            ufb.setMessage(rs.getString(3));
            ufb.setUname(rs.getString(1));
            ufb.setPhoneNo(rs.getLong(2));
            list.add(ufb);
        }
        return list;
    }
    public List<UserFeedBack> getById(String name) throws SQLException
    {
        sql = "SELECT * FROM feedBack where uname=?";
        PreparedStatement pstmt = con.prepareStatement(sql);
        pstmt.setString(1, name);
        // setting up the parameter's for the Query string
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            ufb = new UserFeedBack();
            ufb.setMessage(rs.getString(3));
            ufb.setUname(rs.getString(1));
            ufb.setPhoneNo(rs.getLong(2));
            list.add(ufb);
        }
        return list;
    }
}
