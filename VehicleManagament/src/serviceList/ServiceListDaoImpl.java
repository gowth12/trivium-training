package serviceList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connectionObject.DbUtill;

public class ServiceListDaoImpl implements ServiceListDao
{
    Connection con = DbUtill.getConnection();

    PreparedStatement pstmt = null;

    Statement stmt = null;

    List<ServiceList> list = new ArrayList<ServiceList>();

    ServiceList sl = new ServiceList();

    String sql;

    @Override
    public List<ServiceList> show() throws SQLException
    {
        sql = "select * from servicelist";
        stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        while(rs.next())
        {
            sl = new ServiceList();
            sl.setAddress(rs.getString(4));
            sl.setCashVia(rs.getString(7));
            sl.setId(rs.getInt(1));
            sl.setPhone(rs.getLong(3));
            sl.setPickTime(rs.getString(6));
            sl.setPickUpDate(rs.getString(5));
            sl.setUname(rs.getString(2));
            list.add(sl);
        }
        return list;
    }

    @Override
    public String delete(int id) throws Exception
    {
        sql = "delete from servicelist where id=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setInt(1, id);
        if(pstmt.executeUpdate() != 0)
            return "sucessfully deleted";
        else
            return "failed to delete";
    }
    
    @Override
    public String deleteByPhone(long phone) throws Exception
    {
        sql = "delete from servicelist where phoneNo=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setLong(1, phone);
        if(pstmt.executeUpdate() != 0)
            return "sucessfully deleted";
        else
            return "failed to delete";
    }
    
    public String deleteByPhone(int id) throws Exception
    {
        sql = "delete from servicelist where id=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setLong(1, id);
        if(pstmt.executeUpdate() != 0)
            return "sucessfully deleted";
        else
            return "failed to delete";
    }


    @Override
    public List<ServiceList> getById(int id) throws SQLException
    {
        sql = "SELECT * FROM ServiceList where id=?";
        PreparedStatement pstmt = con.prepareStatement(sql);
        pstmt.setInt(1, id);
        // setting up the parameter's for the Query string
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            sl = new ServiceList();
            sl.setAddress(rs.getString(4));
            sl.setCashVia(rs.getString(7));
            sl.setId(rs.getInt(1));
            sl.setPhone(rs.getLong(3));
            sl.setPickTime(rs.getString(6));
            sl.setPickUpDate(rs.getString(5));
            sl.setUname(rs.getString(2));
            list.add(sl);
        }
        return list;
    }
    
   
    public List<ServiceList> getById(String name) throws SQLException
    {
        sql = "SELECT * FROM ServiceList where uname=?";
        PreparedStatement pstmt = con.prepareStatement(sql);
        pstmt.setString(1, name);
        // setting up the parameter's for the Query string
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            sl = new ServiceList();
            sl.setAddress(rs.getString(4));
            sl.setCashVia(rs.getString(7));
            sl.setId(rs.getInt(1));
            sl.setPhone(rs.getLong(3));
            sl.setPickTime(rs.getString(6));
            sl.setPickUpDate(rs.getString(5));
            sl.setUname(rs.getString(2));
            list.add(sl);
        }
        return list;
    }

    @Override
    public String update(ServiceList sl) throws SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String create(ServiceList sl) throws SQLException
    {
        sql = "insert into servicelist(uname,phoneNo,address,pickupdate,time,cashvia) values(?,?,?,?,?,?)";
        pstmt = con.prepareStatement(sql);
        pstmt.setString(1, sl.getUname());
        pstmt.setLong(2, sl.getPhone());
        pstmt.setString(3, sl.getAddress());
        pstmt.setString(4, sl.getPickUpDate());
        pstmt.setString(5, sl.getPickTime());
        pstmt.setString(6, sl.getCashVia());
        if(pstmt.executeUpdate() != 0)
            return "sucessfully added";
        else
            return "failed to add";
    }
}
