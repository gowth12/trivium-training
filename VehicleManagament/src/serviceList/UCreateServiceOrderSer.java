package serviceList;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UCreateServiceOrderSer")
public class UCreateServiceOrderSer extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PrintWriter out=response.getWriter();
        ServiceList sl = new ServiceList();
        ServiceListDaoImpl sldi = new ServiceListDaoImpl();
        response.setContentType("text/html");
        String name = request.getParameter("txtName");
        String phoneNo = request.getParameter("txtPhoneNo");
        String address = request.getParameter("txtAddress");
        String date = request.getParameter("txtPDate");
        String time = request.getParameter("txtPTime");
        String payment = request.getParameter("txtPayment");
        long phoneNo1 = Long.parseLong(phoneNo);
        sl.setAddress(address);
        sl.setCashVia(payment);
        sl.setPhone(phoneNo1);
        sl.setPickTime(time);
        sl.setPickUpDate(date);
        sl.setUname(name);
        String msg;
        try
        {
            msg = sldi.create(sl);
            if(msg == "sucessfully added")
            {
                out.println("<script type=\"text javascript\">");
            out.println("alert('Sucessfully created');");
            out.println("</script>");
         
                response.sendRedirect("UChoice.html");
            }
            else
            {
                out.println("<html><body bgcolor=green>");
                out.println("<script type=\"text/javascript\">");
                out.println("alert('failed to create');");
                out.println("</script>");
                out.println("</body></html>");
            }
                
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
}
