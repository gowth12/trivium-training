
package userRegistration;

/**
 * @author Goutham.Himachala
 *  This class is used to generate pojo objects on user UserRegistration
 */
public class UserRegistration
{
    private String uName;

    private String fName;

    private String lName;

    private String password;

    private String dob;

    private String gender;

    private long phoneNo;

    private String address;

    private String email;

    private long drivingLicenseNo;

    private String vehicleNo;

    public String getuName()
    {
        return uName;
    }

    @Override
    public String toString()
    {
        return "UserRegistration [uName=" + uName + ", fName=" + fName + ", lName=" + lName + ", password=" + password
                + ", dob=" + dob + ", gender=" + gender + ", phoneNo=" + phoneNo + ", address=" + address + ", email="
                + email + ", drivingLicenseNo=" + drivingLicenseNo + ", vehicleNo=" + vehicleNo + "]";
    }

    public void setuName(String uName)
    {
        this.uName = uName;
    }

    public String getfName()
    {
        return fName;
    }

    public void setfName(String fName)
    {
        this.fName = fName;
    }

    public String getlName()
    {
        return lName;
    }

    public void setlName(String lName)
    {
        this.lName = lName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getDob()
    {
        return dob;
    }

    public void setDob(String dob)
    {
        this.dob = dob;
    }

    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public long getPhoneNo()
    {
        return phoneNo;
    }

    public void setPhoneNo(long phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public long getDrivingLicenseNo()
    {
        return drivingLicenseNo;
    }

    public void setDrivingLicenseNo(long drivingLicenseNo)
    {
        this.drivingLicenseNo = drivingLicenseNo;
    }

    public String getVehicleNo()
    {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo)
    {
        this.vehicleNo = vehicleNo;
    }
}
