package userRegistration;

/**
 * @author Goutham.Himachala
 *  This servlet searches the  user based on phoneNo
 */
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminSearchUser
 */
@WebServlet("/AdminSearchUser")
public class AdminSearchUser extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html");
        UserRegistration u = new UserRegistration();
        UserRegistrationDaoImpl urdi = new UserRegistrationDaoImpl();
        String phoneNo = request.getParameter("txtSearch");
        long phone = Long.parseLong(phoneNo);
        u.setPhoneNo(phone);
        try
        {
            List<UserRegistration> result = urdi.getByPhoneNo(u.getPhoneNo());
            if(result.size() != 0)
                response.sendRedirect("ASearch.jsp");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
}
