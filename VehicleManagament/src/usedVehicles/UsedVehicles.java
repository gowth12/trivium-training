package usedVehicles;
/**
 * @author Goutham.Himachala 
 * This class is used to generate pojo objects on user Used Vhicles
 */
public class UsedVehicles
{
    private int id;

    private String Vname;

    private String model;

    private double mielage;

    private String doccsAvailable;

    private String condition;

    private long phone;

    @Override
    public String toString()
    {
        return "UsedVehicles [id=" + id + ", Uname=" + Vname + ", model=" + model + ", mielage=" + mielage
                + ", doccsAvailable=" + doccsAvailable + ", condition=" + condition + ", phone=" + phone + "]";
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getVname()
    {
        return Vname;
    }

    public void setVname(String vname)
    {
        Vname = vname;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public double getMielage()
    {
        return mielage;
    }

    public void setMielage(double mielage)
    {
        this.mielage = mielage;
    }

    public String getDoccsAvailable()
    {
        return doccsAvailable;
    }

    public void setDoccsAvailable(String doccsAvailable)
    {
        this.doccsAvailable = doccsAvailable;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public long getPhone()
    {
        return phone;
    }

    public void setPhone(long phone)
    {
        this.phone = phone;
    }
}
