package autoMobiles;

/**
 * This class is used to perform Testing on AutoMobiles
 * 
 * @author Goutham.Himachala
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.sql.SQLException;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.Disabled;

public class TestAutoMobilesDaoImpl
{
    AutoMobiles am = new AutoMobiles();

    AutoMobilesDaoImpl amdi = new AutoMobilesDaoImpl();

    /**
     * this is used for testing method Craete
     */
    @Disabled
    public void testCraete()
    {
        am.setName("kk");
        am.setPrice(666.6);
        am.setProductCompany("ytgfd");
        am.setVehicleModel(2011);
        am.setVehicleType("car");
        am.setWarranty(2.2);
        try
        {
            assertEquals(amdi.create(am), "sucessfully added");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * this is used for testing method Delete
     */
    @Disabled
    public void testDelete()
    {
        am.setId(15);
        int p = am.getId();
        try
        {
            assertEquals(amdi.delete(p), "sucessfully deleted");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * this is used for testing method Update
     */
    @Disabled
    public void testUpdate()
    {
        am.setId(9);
        am.setName("dsd");
        am.setPrice(99.33);
        am.setProductCompany("sdd");
        am.setVehicleModel(2016);
        am.setVehicleType("bike");
        am.setWarranty(7.9);
        try
        {
            assertEquals(amdi.update(am), "sucessfully updated");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * this is used for testing method GetById
     */
    @Disabled
    public void testGetById()
    {
        am.setId(112);
        int id = am.getId();
        List<AutoMobiles> li;
        try
        {
            li = amdi.getById(id);
            assertTrue(!li.isEmpty());
        }
        catch(SQLException e1)
        {
            e1.printStackTrace();
        }
    }

    /**
     * this is used for testing method Show
     */
    @Test
    public void testShow() throws SQLException
    {
        List<AutoMobiles> li;
        li = amdi.show();
        assertTrue(li.size() != 0);
    }
}
