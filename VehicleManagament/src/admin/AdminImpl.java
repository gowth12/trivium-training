package admin;

/**
 * @author Goutham.Himachala 
 * This class checks the userId and password of admin with the database
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import connectionObject.DbUtill;

public class AdminImpl
{
    PreparedStatement pstmt = null;

    Connection con = DbUtill.getConnection();

    /***
     * This method checks the data recieved from the admin and returns true if it is valid else
     * return false
     * 
     * @param a
     * @return true or false
     * @throws SQLException
     */
    public boolean validateLogin(Admin a) throws SQLException
    {
        String sql = "select * from Admin where uName=? and password=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setString(1, a.getUname());
        pstmt.setString(2, a.getPassword());
        ResultSet rs = pstmt.executeQuery();
        if(rs.next())
            return true;
        else
            return false;
    }
}
