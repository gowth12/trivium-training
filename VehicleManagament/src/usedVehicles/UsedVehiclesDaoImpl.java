package usedVehicles;

/**
 * @author Goutham.Himachala 
 *  This class is used to perform opertaion on UsedVehicles
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connectionObject.DbUtill;

public class UsedVehiclesDaoImpl implements UsedVehiclesDao
{
    Connection con = DbUtill.getConnection();

    List<UsedVehicles> list = new ArrayList<UsedVehicles>();

    PreparedStatement pstmt = null;

    UsedVehicles uv = new UsedVehicles();

    String sql;

    /**
     * This method is used to retireve all the used vehicles
     */
    @Override
    public List<UsedVehicles> show() throws SQLException
    {
        sql = "select * from usedVehicles";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        while(rs.next())
        {
            uv = new UsedVehicles();
            uv.setMielage(rs.getDouble(4));
            uv.setPhone(rs.getLong(7));
            uv.setId(rs.getInt(1));
            uv.setModel(rs.getString(3));
            uv.setCondition(rs.getString(6));
            uv.setDoccsAvailable(rs.getString(5));
            uv.setVname(rs.getString(2));
            list.add(uv);
        }
        return list;
    }

    /**
     * This methos deletes the data by accepting the unique id
     */
    @Override
    public String delete(int id) throws Exception
    {
        sql = "delete from usedvehicles where id=?";
        pstmt = con.prepareStatement(sql);
        pstmt.setInt(1, id);
        if(pstmt.executeUpdate() != 0)
            return "sucessfully deleted";
        else
            return "failed to delete";
    }

    /**
     * This method is used to retireve the list of auto mobile parts based on id
     */
    @Override
    public List<UsedVehicles> getById(int id) throws SQLException
    {
        sql = "SELECT * FROM usedvehicles where id=?";
        PreparedStatement pstmt = con.prepareStatement(sql);
        pstmt.setInt(1, id);
        // setting up the parameter's for the Query string
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
            uv = new UsedVehicles();
            uv.setMielage(rs.getDouble(4));
            uv.setPhone(rs.getLong(7));
            uv.setId(rs.getInt(1));
            uv.setModel(rs.getString(3));
            uv.setCondition(rs.getString(6));
            uv.setDoccsAvailable(rs.getString(5));
            uv.setVname(rs.getString(2));
            list.add(uv);
        }
        return list;
    }

    /**
     * This method is used for the updating the existing data with new data for the particular user
     */
    @Override
    public String update(UsedVehicles uv) throws SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * This method inserts the data in to the table UserRegistration
     */
    @Override
    public String create(UsedVehicles uv) throws SQLException
    {
        sql = "insert into usedVehicles(vname,model,mielage,availabiltyofdoccs,condition,phoneNo) values(?,?,?,?,?,?)";
        pstmt = con.prepareStatement(sql);
        pstmt.setString(1, uv.getVname());
        pstmt.setString(2, uv.getModel());
        pstmt.setDouble(3, uv.getMielage());
        pstmt.setString(4, uv.getDoccsAvailable());
        pstmt.setString(5, uv.getCondition());
        pstmt.setLong(6, uv.getPhone());
        if(pstmt.executeUpdate() != 0)
            return "sucessfully added";
        else
            return "failed to add";
    }
}
